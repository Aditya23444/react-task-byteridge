import React from 'react';
import { connect } from 'react-redux';
import {
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
} from '@material-ui/core';

import { userLogActions} from '../_actions';


class AuditPage extends React.Component {

    componentDidMount() {
        this.props.dispatch(userLogActions.getAllAudit(0))
    }


    render() {
        const {users} = this.props;

        return (
            <div>


                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="right" > Name</TableCell>
                                <TableCell align="right">user Name</TableCell>
                                <TableCell align="right">Login Time</TableCell>
                                <TableCell align="right">Logout Time</TableCell>
                                <TableCell align="right">Client IP</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                users && users.items && users.items.map((userLog) =>
                                    <TableRow>
                                        <TableCell align="right">{userLog.userId.firstName + ' '+ userLog.userId.lastName}</TableCell>
                                        <TableCell align="right">{userLog.userId.username}</TableCell>
                                        <TableCell align="right">{userLog.loginTime}</TableCell>
                                        <TableCell align="right">{userLog.logoutTime ?userLog.logoutTime : 'Not available'}</TableCell>
                                        <TableCell align="right">{userLog.clientIp}</TableCell>

                                    </TableRow>
                                )
                            }
                        </TableBody>
                    </Table>
            </div>
        );
    }

}


function mapState(state) {
    const { users } = state;
    return {  users };
}

const connectedAuditPage = connect(mapState)(AuditPage);
export { connectedAuditPage as AuditPage };